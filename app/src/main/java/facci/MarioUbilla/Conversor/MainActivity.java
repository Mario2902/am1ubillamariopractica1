package facci.MarioUbilla.Conversor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    TextView RC;
    TextView RF;

    //inputs
    TextInputEditText celsios;
    TextInputEditText farenheist;

    Button cambio_c;
    Button cambio_f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //variables de text
        RC = findViewById(R.id.Rc);
        RF = findViewById(R.id.Rf);

        celsios = findViewById(R.id.inputcelsius);
        farenheist = findViewById(R.id.inputfarenheist);

        cambio_c = findViewById(R.id.button);
        cambio_f = findViewById(R.id.button2);

        //escuchar evento

        cambio_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cTof();
            }
        });

        cambio_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fToc();
            }
        });
    }



    private void cTof(){
        double g_celcios=Double.parseDouble(String.valueOf(celsios.getText()));
        double g_farenheist = (g_celcios*9/5)+32;
        g_farenheist = Math.round(g_farenheist * 100.0) / 100.0;
        RC.setText(String.valueOf(g_farenheist)+" °F");
    }

    private void fToc(){
        double g_farenheist=Double.parseDouble(String.valueOf(farenheist.getText()));
        double g_celcios = (g_farenheist-32)*5/9;
        g_celcios = Math.round(g_celcios * 100.0) / 100.0;
        RF.setText(String.valueOf(g_celcios)+ " °C");
    }
}